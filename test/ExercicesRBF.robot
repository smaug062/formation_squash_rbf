
*** Settings ***
Resource	squash_resources.resource
Library    SeleniumLibrary

Test Setup    Test Setup
Test Teardown    Test Teardown


*** Keywords ***
Test Setup
    Open Browser    ${url}    ${browser}
    Maximize Browser Window

Test Teardown
    Sleep    5
    Close All Browsers

*** Test Cases ***
BDD1 - Connexion à l'application
    [Tags]    run_BDD1
	Given L'utilisateur est sur la page d'accueil
	When L'utilisateur souhaite prendre un rendez-vous
	Then La page de connexion s'affiche
	When L'utilisateur se connecte    ${User1.login}    ${User1.pwd}
	Then L'utilisateur est connecté sur la page de rendez-vous


BDD2_Case_01_HealthCareProgram - Medicare
    [Tags]    run_BDD2
    Given Je suis connecté sur la page de prise de rendez-vous
	When Je renseigne les informations obligatoires
	And Je choisis    ${radioHealthcareMedicare}
	And Je clique sur BookAppointment
	Then Le rendez-vous est confirmé et est affiché    Medicare


BDD2_Case_02_HealthCareProgram - Medicaid
    [Tags]    run_BDD2
    Given Je suis connecté sur la page de prise de rendez-vous
	When Je renseigne les informations obligatoires
	And Je choisis    ${radioHealthcareMedicaid}
	And Je clique sur BookAppointment
	Then Le rendez-vous est confirmé et est affiché    Medicaid


BDD2_Case_03_HealthCareProgram - None
    [Tags]    run_BDD2
    Given Je suis connecté sur la page de prise de rendez-vous
	When Je renseigne les informations obligatoires
	And Je choisis    ${radioHealthcareNone}
	And Je clique sur BookAppointment
	Then Le rendez-vous est confirmé et est affiché    None



BDD3_Case_01_HealthCareProgram - JDD1
    [Tags]    run_BDD3
    Given Je suis connecté sur la page de prise de rendez-vous
	When Je renseigne les informations obligatoires
	And Je choisis elements    ${JDD1.facility}    ${JDD1.HealthCareProgram}    ${JDD1.hospital_readmission}
	And Je clique sur BookAppointment
	Then Le rendez-vous est confirmé    ${JDD1.verifHealthCareProgram}    ${JDD1.facility}    ${JDD1.hospital_readmission}


BDD3_Case_02_HealthCareProgram - JDD2
    [Tags]    run_BDD3
    Given Je suis connecté sur la page de prise de rendez-vous
	When Je renseigne les informations obligatoires
	And Je choisis elements    ${JDD2.facility}    ${JDD2.HealthCareProgram}    ${JDD2.hospital_readmission}
	And Je clique sur BookAppointment
	Then Le rendez-vous est confirmé    ${JDD2.verifHealthCareProgram}    ${JDD2.facility}     ${JDD2.hospital_readmission}


BDD3_Case_03_HealthCareProgram - JDD3
    [Tags]    run_BDD3
    Given Je suis connecté sur la page de prise de rendez-vous
	When Je renseigne les informations obligatoires
	And Je choisis elements    ${JDD3.facility}    ${JDD3.HealthCareProgram}    ${JDD3.hospital_readmission}
	And Je clique sur BookAppointment
	Then Le rendez-vous est confirmé    ${JDD3.verifHealthCareProgram}    ${JDD3.facility}    ${JDD3.hospital_readmission}
